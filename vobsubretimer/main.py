#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
'''
 * Copyright (C) 2009-2016, Mozbugbox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301,
 * USA.
'''

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
import json

import mpv
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gio, Gdk, GdkPixbuf

__version__ = "0.1.0"

TEN6 = 1000000 # 10**6
TEN3 = 1000 # 10**3
NATIVE=sys.getfilesystemencoding()
class EHand:
    """mpv event handler"""
    def __init__(self, player, app):
        self.player = player
        self.app = app

    def call(self, evt):
        """Use idle_add to callback in main thread"""
        app = self.app
        if evt["event_id"] == mpv.MpvEventID.VIDEO_RECONFIG:
            GLib.idle_add(app.reconfig_video)

def mpv_key(event):
    """Convert GdkKeyEvent to mpv key scheme"""
    kmap = {
            "braceleft": "{",
            "braceright": "}",
            "bracketleft": "[",
            "bracketright": "]",
            "BackSpace": "BS",
            "comma": ",",
            "period": ".",
            "Page_Up": "PGUP",
            "Page_Down": "PGDWN",
            "plus": "+",
            "minus": "-",
            }
    kname = Gdk.keyval_name(event.keyval)
    if kname in kmap:
        kname = kmap[kname]
    elif len(kname) > 1:
        kname = kname.upper()
    mtype = Gdk.ModifierType
    AltCtrlShift = mtype.CONTROL_MASK|mtype.MOD1_MASK|mtype.SHIFT_MASK
    if event.state & AltCtrlShift:
        mods = []
        if event.state & mtype.CONTROL_MASK:
            mods.append("Ctrl")
        if len(kname) > 1 and event.state & mtype.SHIFT_MASK:
            mods.append("Shift")
        if event.state & mtype.MOD1_MASK:
            mods.append("Alt")
        mods.append(kname)
        kname = "+".join(mods)
    return kname

def collect_subtitle(fname):
    """return a list of pixbuf with timestamp"""
    result = []
    from . import vobsub
    vb = vobsub.VobSub(fname)
    s = vb.streams[0]
    for subset in s:
        imgs = [sub.image for sub in subset]
        result.append((subset.pts, imgs))
    return result

def im_to_pixbuf(im):
    """Convert PIL Image to Pixbuf"""
    cmode = "RGB"
    if im.mode != cmode:
        im = im.convert(cmode)
    data = im.tobytes()
    pixels = GLib.Bytes(data)

    pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(pixels,
            GdkPixbuf.Colorspace.RGB, False, 8,
            im.width, im.height,
            len(data)/im.height)
    return pixbuf

def vobsub_time(microsec):
    """convert microseconds to timestamp format of vobsub"""
    #print(type(microsec), microsec)
    microsec = int(microsec)
    seconds = microsec // TEN6
    micros = microsec % TEN6
    hours = seconds // 3600
    rd = seconds % 3600
    minutes = rd // 60
    seconds = rd % 60
    milliseconds = micros // TEN3
    vtime = "{:02d}:{:02d}:{:02d}:{:-03d}".format(hours, minutes, seconds,
            milliseconds)
    return vtime

def from_vobsub_time(timestamp):
    """convert vobsub timestamp (00:00:00:000) to microsec"""
    tparts = timestamp.strip(",").split(":")
    tparts = [int(x) for x in tparts]
    t = 3600 * tparts[0] + 60 * tparts[1] + tparts[2] # in seconds
    t = t * TEN6 + tparts[3] * TEN3 # with msec in usec
    return t

def _create_openfile_dialog(title, parent=None, filters=None):
    """create a file chooser dialog"""
    dlg_fopen = Gtk.FileChooserDialog(title, parent,
            Gtk.FileChooserAction.OPEN, [
                "_Cancel", Gtk.ResponseType.CANCEL,
                "_Open", Gtk.ResponseType.ACCEPT,
                ])
    dlg_fopen.props.mnemonics_visible = True

    if not filters:
        filters = []
    filters.append(("All", ["*.*"]))
    for name, patterns in filters:
        ff = Gtk.FileFilter()
        ff.set_name(name)
        for p in patterns:
            ff.add_pattern(p)
        dlg_fopen.add_filter(ff)
    return dlg_fopen

def _create_new_project_dialog(parent=None, video_name="", sub_name=""):
    """On create a new project, query video and subtitle file names"""
    max_chars = 60
    dlg = Gtk.Dialog("Create Subtitle Retiming Project", parent,
            0, ( "_OK", Gtk.ResponseType.OK,))
    grid = Gtk.Grid()
    grid.props.column_spacing = 6
    grid.props.row_spacing = 4
    dlg.get_content_area().add(grid)

    def _do_open_file(btn, entry_wid, title, filters):
        dlg_fopen = _create_openfile_dialog(title, dlg, filters)
        resp = dlg_fopen.run()
        if resp == Gtk.ResponseType.ACCEPT:
            fname = dlg_fopen.get_filename()
            entry_wid.set_text(fname)
        dlg_fopen.destroy()

    label_v = Gtk.Label("Video:")
    label_v.props.xalign = 0.0
    entry_v = Gtk.Entry()
    entry_v.props.max_width_chars = max_chars
    if video_name:
        entry_v.set_text(video_name)
    button_v = Gtk.Button("Open")
    grid.attach(label_v, 0, 0, 1, 1)
    grid.attach(entry_v, 1, 0, 1, 1)
    grid.attach(button_v, 2, 0, 1, 1)
    button_v.connect("clicked", _do_open_file, entry_v,
            "Open Video File...", None)

    label_s = Gtk.Label("Subtitle (*.idx):")
    label_s.props.xalign = 0.0
    entry_s = Gtk.Entry()
    entry_s.props.max_width_chars = max_chars
    if sub_name:
        entry_s.set_text(sub_name)

    button_s = Gtk.Button("Open")
    grid.attach(label_s, 0, 1, 1, 1)
    grid.attach(entry_s, 1, 1, 1, 1)
    grid.attach(button_s, 2, 1, 1, 1)
    button_s.connect("clicked", _do_open_file, entry_s,
            "Open Subtitle idx...",
            [(".idx", ["*.idx"])])

    dlg.show_all()
    resp = dlg.run()

    res = None
    if resp == Gtk.ResponseType.OK:
        vname = entry_v.get_text()
        sname = entry_s.get_text()
        if vname and sname:
            res = (vname, sname)
    dlg.destroy()
    return res

def _create_icon_button(icon_name=None, tooltip=None, action=None):
    """Create Gtk button with stock icon"""
    bt = Gtk.Button()
    bt.props.can_focus = False
    if icon_name is not None:
        icon = Gio.ThemedIcon(name=icon_name)
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        bt.add(image)
    if tooltip is not None:
        bt.props.tooltip_text = tooltip
    if action is not None:
        bt.connect("clicked", action)
    return bt

def escape_xml( text ):
    text = text.replace("&", "&amp;")
    text = text.replace("<", "&lt;")
    text = text.replace(">", "&gt;")
    text = text.replace("\"", "&quot;")
    return text

def _shortcut_group(title, shortcut_list):
    header = """
    <object class="GtkShortcutsGroup">
    <property name="visible">1</property>
    <property name="title" translatable="yes">{title:}</property>
{content:}
    </object>
    """
    templ = """
        <child>
          <object class="GtkShortcutsShortcut">
            <property name="visible">1</property>
            <property name="accelerator">{shortcut:}</property>
            <property name="title" translatable="yes">{title:}</property>
          </object>
        </child>
    """
    result = []
    shortcut_list = [(escape_xml(x[0]), escape_xml(x[1]))
        for x in shortcut_list]
    for shortcut, sc_title in shortcut_list:
        result.append(templ.format(shortcut=shortcut, title=sc_title))
    result = "".join(result)
    result = header.format(title=title, content=result)
    return result

def _shortcut_section(secname, group_list):
    header = """
        <object class="GtkShortcutsSection">
        <property name="visible">1</property>
        <property name="section-name">{secname:}</property>
        <property name="max-height">12</property>
        {content:}
        </object>
    """
    temple = "<child>\n{}\n</child>\n"
    result = "".join([temple.format(x) for x in group_list])
    result = header.format(secname=secname, content=result)
    return result

def _shortcut_window(wid, section_list):
    header = """
<?xml version="1.0" encoding="UTF-8"?>
<interface>
<!-- interface-requires gtk+ 3.17 -->

<object class="GtkShortcutsWindow" id="{wid:}">
    <property name="modal">1</property>
    {content:}
</object>
    """
    temple = "<child>\n{}\n</child>\n"
    result = "".join([temple.format(x) for x in section_list])
    result = header.format(wid=wid, content=result)
    return result


# Enum for TreeView model index
(
    COL_INDEX,
    COL_OLD,
    COL_NEW,
    COL_PIX,
    COL_OLD_USEC,
    COL_NEW_USEC,
) = range(6)

class App:
    def __init__(self, video_path, sub_path):
        self.subtitle = None
        self.changed = False
        self.player = None
        self.video_path = video_path
        self.sub_path = sub_path
        self.setup_gui()
        self.load_subtitle(self.sub_path)

        # some how need to delay mpv loader after load subtitle
        # or it crashes at filling treeview content
        GLib.timeout_add_seconds(1, self.setup_player)

        # Save backup file
        GLib.timeout_add_seconds(30, self.periodic_backup)

    def setup_gui(self):
        self.win = win = Gtk.ApplicationWindow()
        self.win.resize(500, 500)
        grid = Gtk.Grid()
        grid.props.expand = True
        win.add(grid)

        hb = self.setup_header_bar()
        win.set_titlebar(hb)

        self.darea = Gtk.DrawingArea()
        self.darea.show()
        self.darea.props.expand = False
        grid.attach(self.darea, 0, 0, 1, 1)

        self.tree = self.setup_tree()
        sw = Gtk.ScrolledWindow()
        sw.props.expand = True
        sw.add(self.tree)
        grid.attach(sw, 0, 1, 1, 1)

        win.connect("delete-event", self.quit)
        win.connect("key-press-event", self.on_key_pressed)
        win.show_all()

    def setup_tree(self):
        """Subtitle display treeview"""
        tree = Gtk.TreeView()
        renderer_old = Gtk.CellRendererText(xalign=0.0, yalign=0.0)
        renderer_new = Gtk.CellRendererText(xalign=0.0, yalign=0.0)
        renderer_index = Gtk.CellRendererText(xalign=0.0, yalign=0.0)
        renderer_pixbuf = Gtk.CellRendererPixbuf(ypad=6)

        column_index = Gtk.TreeViewColumn("#", renderer_index, text=COL_INDEX)
        column_old = Gtk.TreeViewColumn("Old", renderer_old, text=COL_OLD)
        column_new = Gtk.TreeViewColumn("New", renderer_new, text=COL_NEW)
        column_pixbuf = Gtk.TreeViewColumn("Subtitle", renderer_pixbuf,
                pixbuf=COL_PIX)

        column_pixbuf.props.expand = True

        tree.append_column(column_index)
        tree.append_column(column_old)
        tree.append_column(column_new)
        tree.append_column(column_pixbuf)

        # id, old time, new time, subtitle pic, old timestamp, new timestamp
        model = Gtk.ListStore(int, str, str, GdkPixbuf.Pixbuf,
                "glong", "glong")
        tree.set_model(model)
        return tree

    def setup_header_bar(self):
        hb = Gtk.HeaderBar()
        hb.props.show_close_button = True
        hb.props.title = "VobSub ReTimer"
        hb.props.has_subtitle = False

        button_open = _create_icon_button("application-exit-symbolic",
                "Quit", self.quit)
        button_save = _create_icon_button("document-save-symbolic",
                "Save Fixed Subtitle", self.export_result)
        button_ajust_rest = _create_icon_button("face-monkey",
                "Adjust rest time stamp base on current subtitle",
                self.adjust_rest_subtitle_time_to_current)
        button_help = _create_icon_button("help-faq",
                "Help", self.on_help)

        hb.pack_start(button_open)
        hb.pack_start(button_save)
        hb.pack_start(button_ajust_rest)
        hb.pack_end(button_help)
        return hb

    def setup_player(self):
        """Start mpv player"""
        xid = self.darea.props.window.get_xid()
        #xid = self.darea.get_id()
        self.player = self.load_player(self.video_path, xid)

    def load_player(self, fname, wid):
        player = mpv.MPV(input_default_bindings=True,
                input_vo_keyboard=True, wid=wid)
        player.play(fname)
        eh = EHand(player, self)
        self.eh = eh
        player.event_callbacks.append(eh)
        return player

    def load_subtitle(self, fname):
        """load content of subtitle file to treeview"""
        if self.subtitle:
            return
        subs = collect_subtitle(fname)
        with io.open(self.sub_path_backup) as fh:
            backup = json.load(fh)
            backup = {int(x[0]): x[1] for x in backup.items()}
        self.subtitle = subs
        model = self.tree.get_model()
        for i, (timestamp, imgs) in enumerate(subs):
            timestamp_new = -1
            im = imgs[0]
            pixbuf = im_to_pixbuf(im)
            gimg = Gtk.Image()
            gimg.set_from_pixbuf(pixbuf)
            time_old = vobsub_time(timestamp)
            time_new = ""
            if timestamp in backup:
                timestamp_new = backup[timestamp]
                time_new = vobsub_time(timestamp_new)
            #model.append((old_stamp, "", ""))

            model.append((i+1, time_old, time_new, pixbuf,
                timestamp, timestamp_new))
        miter = model.get_iter_first()
        path = model.get_path(miter)
        self.tree.set_cursor(path, None)

    def reconfig_video(self):
        """On video reconfig event"""
        player = self.player
        player.osd_level = 3
        width = self.player.dwidth
        height = self.player.dheight
        self.darea.set_size_request(width, height)
        self.win.resize(width, min(height+300, 765))
        #player.pause = True
        #print(width, height + 300)

    def on_key_pressed(self, widget, event):
        """Keyboard handling"""
        ret = True
        # reserve binding for some of mpv functions
        pass_through_keys = {
                "SPACE", "LEFT", "RIGHT", "UP", "DOWN",
                "[", "]", "{", "}", "BS",
                ".", ",",
                "Shift+PGUP", "Shift+PGDWN",
                "1","2","3","4","5","6","7","8","9","0",
                "Ctrl++", "Ctrl+-",
                }
        kname = Gdk.keyval_name(event.keyval)
        mkey = mpv_key(event)
        #print("kname:", kname)
        #print("mpv key:", mkey)
        mtype = Gdk.ModifierType
        AltCtrlShift = mtype.CONTROL_MASK|mtype.MOD1_MASK|mtype.SHIFT_MASK
        AltShift = mtype.MOD1_MASK|mtype.SHIFT_MASK
        with_altctrlshift = event.state & AltCtrlShift
        ctrl_only = (with_altctrlshift & AltShift) == 0

        if mkey in pass_through_keys:
            #print(mkey)
            if self.player:
                self.player.command("keypress", mkey)

        elif with_altctrlshift == 0:
            if kname == "p": # restart the stopped player
                self.replay()

            elif kname == "h":
                self.player.command("seek", "-3", "exact")
            elif kname == "l":
                self.player.command("seek", "3", "exact")

            elif kname == "g":
                self.goto_subtitle_time()

            elif kname == "k":
                self.previous_subtitle()
            elif kname == "j":
                self.next_subtitle()

            elif kname == "s":
                self.set_subtitle_time()
            elif kname == "u":
                self.clear_subtitle_time()

            elif kname == "e":
                self.change_subtitle_time(-100)
            elif kname == "r":
                self.change_subtitle_time(100)
            elif kname == "d":
                self.change_subtitle_time(-200)
            elif kname == "f":
                self.change_subtitle_time(200)
            elif kname == "c":
                self.change_subtitle_time(-300)
            elif kname == "v":
                self.change_subtitle_time(300)
            elif kname == "F1":
                self.on_help()
            else:
                ret = False

        elif kname == "r" and ctrl_only:
            self.adjust_rest_subtitle_time_to_current()
        elif kname == "s" and ctrl_only:
            self.export_result()
        elif kname == "q" and ctrl_only:
            self.quit()
        else:
            ret = False
        return ret

    def replay(self):
        """Restart mpv player when it stopped"""
        self.player.play(self.video_path)

    def goto_subtitle_time(self):
        """set player time to current selected subtitle time"""
        model = self.tree.get_model()
        path, col = self.tree.get_cursor()
        ptime = model[path][COL_NEW_USEC]
        if ptime < 0:
            ptime = model[path][COL_OLD_USEC]

        mtime = ptime/TEN6
        if mtime > self.player.length:
            mtime = self.player.length - 10
        self.player.time_pos = mtime

    def next_subtitle(self):
        """go to next subtitle"""
        path, col = self.tree.get_cursor()
        if path:
            path.next()
            model = self.tree.get_model()
            try:
                model.get_iter(path)
                self.tree.set_cursor(path)
            except ValueError:
                pass

    def previous_subtitle(self):
        """go to previous subtitle"""
        path, col = self.tree.get_cursor()
        if path:
            if path.prev():
                self.tree.set_cursor(path)

    def set_subtitle_time(self):
        """set current player time to subtitle time"""
        ptime = int(self.player.time_pos * TEN6)
        model = self.tree.get_model()
        path, col = self.tree.get_cursor()
        model[path][COL_NEW] = vobsub_time(ptime)
        model[path][COL_NEW_USEC] = ptime
        self.changed = True
        #print(model[path][-2:])

    def change_subtitle_time(self, inc_msec):
        """inc_msec increase milliseconds"""
        model = self.tree.get_model()
        path, col = self.tree.get_cursor()
        if model[path][COL_NEW_USEC] > 0:
            ptime = model[path][COL_NEW_USEC] + inc_msec * TEN3
            if ptime < 0:
                ptime = 0

            mtime = ptime/TEN6
            if mtime > self.player.length:
                mtime = self.player.length - 10
                ptime = mtime * TEN6
            model[path][COL_NEW] = vobsub_time(ptime)
            model[path][COL_NEW_USEC] = ptime
            self.changed = True
            self.player.time_pos = mtime

    def clear_subtitle_time(self):
        """clear the modified time of the current subtitle """
        model = self.tree.get_model()
        path, col = self.tree.get_cursor()
        model[path][COL_NEW] = ""
        model[path][COL_NEW_USEC] = -1
        self.changed = True

    def adjust_rest_subtitle_time_to_current(self, *event):
        """
        Adjust rest subtitle times based on current subtitle time
        """
        mdlg = Gtk.MessageDialog(self.win, type=Gtk.MessageType.QUESTION,
                buttons=[
                    "Canel", Gtk.ResponseType.CANCEL,
                    "Adjust", Gtk.ResponseType.OK])
        mdlg.props.text = """\
Ajust rest time stamps based on current subtitle?
This action is NOT invertible!!!\
"""
        mdlg.set_title("Confirm Ajusting Rest TimeStamp...")
        resp = mdlg.run()
        mdlg.destroy()
        if resp != Gtk.ResponseType.OK:
            return

        model = self.tree.get_model()
        path, col = self.tree.get_cursor()
        if path:
            miter = model.get_iter(path)
            time_new = model[miter][COL_NEW_USEC]
            time_old = model[miter][COL_OLD_USEC]
            self.tree.freeze_child_notify()
            while True:
                miter = model.iter_next(miter)
                if not miter:
                    break
                time_old_next = model[miter][COL_OLD_USEC]
                time_delta = time_old_next - time_old
                time_new_next = time_new + time_delta
                model[miter][COL_NEW_USEC] = time_new_next
                model[miter][COL_NEW] = vobsub_time(time_new_next)
            self.tree.thaw_child_notify()
            self.changed = True

    @property
    def sub_path_backup(self):
        """Return a filename for writing out backup file"""
        bname = self.sub_path + ".backup"
        return bname

    @property
    def sub_path_new(self):
        """Return a filename for writing out fixed subtitle file"""
        bname = self.sub_path + "-fixed.idx"
        return bname

    def periodic_backup(self):
        """Write backup data periodically"""
        if self.changed:
            self.backup()
            self.changed = False
        return True

    @property
    def changed_timestamp(self):
        """return a dict of changed timestamps: {oldstamp: newstamp}"""
        fixed = {x[COL_OLD_USEC]: x[COL_NEW_USEC]
                for x in self.tree.get_model() if x[COL_NEW_USEC] > 0 }
        return fixed

    def backup(self):
        """Backup changed timestamp to json file"""
        bname = self.sub_path_backup
        with io.open(bname, "w") as fh:
            json.dump(self.changed_timestamp, fh)

    def export_result(self, *args):
        """write fixed subtitle result to disk"""
        content = []
        fixed = self.changed_timestamp
        def fixed_timestamp(aline):
            parts = aline.split()
            if len(parts) != 4 and parts[2] != "filepos:":
                return aline
            timestamp_old = from_vobsub_time(parts[1])
            if timestamp_old in fixed:
                timestamp_new = vobsub_time(fixed[timestamp_old])
                parts[1] = timestamp_new + ","
                aline = " ".join(parts) + "\n"
            return aline

        newlines = None
        with io.open(self.sub_path, encoding="UTF-8") as fh:
            for line in fh:
                if line.startswith("timestamp: "):
                    line = fixed_timestamp(line)
                content.append(line)
            newlines = fh.newlines

        with io.open(self.sub_path_new, "w", encoding="UTF-8",
                newline=newlines) as fh:
            fh.write("".join(content))

        mdlg = Gtk.MessageDialog(self.win, buttons=Gtk.ButtonsType.OK)
        mdlg.props.text = 'Exported to "{}"'.format(self.sub_path_new)
        mdlg.connect("response", lambda x,*y: x.destroy())
        mdlg.show_all()
        return

    def on_help(self, *args):
        app_list = [
                ("<ctrl>s", "Export/Save fixed subtitle"),
                ("<ctrl>q", "Quit Application"),
                ("F1", "Help on shortcut"),
                ]
        player_list = [
                ("p", "Restart player"),
                ("h", "Seek backward 3 sec"),
                ("l", "Seek forward 3 sec"),
                ("g", "Goto subtitle time"),
                ]
        sub_list = [
                ("k", "Previous subtitle"),
                ("j", "Next subtitle"),
                ("s", "Set subtitle to current play time"),
                ("u", "Unset subtitle time"),

                ("e", "Decrease subtitle time by 100ms"),
                ("r", "Increase subtitle time by 100ms"),
                ("d", "Decrease subtitle time by 200ms"),
                ("f", "Increase subtitle time by 200ms"),
                ("c", "Decrease subtitle time by 300ms"),
                ("v", "Increase subtitle time by 300ms"),

                ("<ctrl>r",
                    "Adjust rest time stamp based on current subtitlte"),
                ]
        win_id = "sc-vobsub-retimer"
        app_group = _shortcut_group("Application", app_list)
        player_group = _shortcut_group("Player", player_list)
        sub_group = _shortcut_group("Subtitle Editing", sub_list)
        section_sc = _shortcut_section("Shortcuts",
                [app_group, player_group, sub_group])
        sc_win_text = _shortcut_window(win_id, [section_sc])
        #print(sc_win_text)

        builder = Gtk.Builder()
        builder.add_from_string(sc_win_text)
        sc_win = builder.get_object(win_id)
        sc_win.show_all()

    def run(self):
        Gtk.main()

    def quit(self, *args):
        if self.changed:
            self.backup()
            self.changed = False
        Gtk.main_quit()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    if len(sys.argv) == 3:
        fname = sys.argv[1]
        subname = sys.argv[2]
    elif len(sys.argv) < 3:
        fname = None
        if len(sys.argv) == 2:
            fname = sys.argv[1]
        res = _create_new_project_dialog(video_name=fname)
        if res:
            fname = res[0]
            subname = res[1]
        else:
            print("Please select video and subtitle files.")
            return 2
    else:
        print("Usages: {} video_file subtitle.idx".format(sys.argv[0]))
        return 2

    app = App(fname, subname)
    app.run()

if __name__ == '__main__':
    import signal; signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()

