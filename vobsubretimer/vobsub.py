#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
'''
 * Copyright (C) 2009-2016, Mozbugbox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301,
 * USA.
'''

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
import functools

NATIVE=sys.getfilesystemencoding()

import av
from PIL import Image, ImageOps

def gray_image(w, h, data):
    """Generate grayscale PIL.Image"""
    img = Image.frombuffer('L', (w, h), data, "raw", "L", 0, 1)
    img = ImageOps.invert(ImageOps.equalize(img))
    return img

def sub_to_image(sub):
    """@sub: av.subtitle.Subtitle"""
    buffers = [b for b in sub.planes if b is not None]
    if buffers:
        imgs = [gray_image(sub.width, sub.height, buf) for buf in buffers]
        if len(imgs) == 1:
            img = imgs[0]
        elif len(imgs) == 2:
            img = Image.merge('LA', imgs)
        else:
            img = Image.merge('RGBA', imgs)
        return img

class SubtitleSet:
    def __init__(self, subset, subtitles=[]):
        """subtitle set from av.subtitle.SubtitleSet
        """
        self.start_display_time = subset.start_display_time
        self.end_display_time = subset.end_display_time
        self.pts = subset.pts
        self.subtitles = subtitles

    def __iter__(self):
        return iter(self.subtitles)

class Subtitle:
    def __init__(self, sub):
        """subtitle from av.subtitle.Subtitle"""
        self.x = sub.x
        self.y = sub.y
        self.width = sub.width
        self.height = sub.height
        self.nb_colors = sub.nb_colors
        self.image = sub_to_image(sub)

class SubtitleStream:
    stream_attrs = ["id", "index", "language", "long_name", "name",
            "metadata", "profile", "start_time", "time_base"]
    def __init__(self, video, stream):
        self.video = video
        self.stream = stream
        for name in self.stream_attrs:
            v = getattr(stream, name)
            setattr(self, name, v)

    def __iter__(self):

        def iter_subset():
            """A SubtitleSet generator"""
            video = self.video
            for packet in video.demux(self.stream):
                for subset in packet.decode():
                    sub_list = [Subtitle(sub) for sub in subset
                            if sub.type.decode("UTF-8") == "bitmap"]
                    aset = SubtitleSet(subset, sub_list)
                    yield aset

        return iter_subset()

class VobSub:
    def __init__(self, fname):
        self.fname = fname
        self.video = None
        self.load_subtitle()

    def load_subtitle(self):
        self.video = av.open(self.fname)

    @property
    def streams(self):
        video = self.video
        streams = video.streams
        sstreams = [SubtitleStream(video, s) for s in streams
                if s.type == 'subtitle']
        return sstreams

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)
    fname = sys.argv[1]
    vs = VobSub(fname)
    for s in vs.streams:
        for i, subset in enumerate(s):
            print(i, subset, subset.subtitles)
            for sub in subset:
                print(i, sub.image, subset.pts*s.time_base)
                #sub.image.save("text.png"); return

if __name__ == '__main__':
    main()

