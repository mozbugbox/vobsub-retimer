Vobsub Retimer
==============

| Copyright (C) 2009-2016, Mozbugbox <mozbugbox@yahoo.com.au>
| Release Under GNU GPL version 3 or later version.

A Linux Tool to adjust/edit time stamp of Vobsub(idx/sub) image based subtitle file.

The PyAV_ library was used to parse vobsub subtitle files.

The `python-mpv`_ library was used to play video files.

The intent of this application was to put together a quick tool that I can use
to adjust the time stamp of an out of sync vobsub subtitle file. I cannot find
any subtitle editor that are willing to handle image subtitle for me at the
time, on the Linux platform.

Patches/Pull Requests are welcomed and appreciated!

Features
========

* Display Video and a list of Subtitle content.

* Keyboard shortcut to set/increase/decrease subtitle time stamp

* Backup file while time stamp changed

Usage
=====
Usage::

    $ vobsub-retimer video-file.mkv subtitle.idx
    
While playing the video, press "``s``" key to set the new timestamp for selected subtitle. 

Press "``F1``" key for shortcut key mapping.

Dependency
==========

* Python_ 3

* PyAV_: Load vobsub title file

* `python-mpv`_: Play video with mpv_

* `GTK+`_ / PyGObject_: GUI interface

* Pillow_: Process image data from PyAV_ 

Homepage
========

URL: https://gitlab.com/mozbugbox/vobsub-retimer


.. _Python: https://www.python.org/
.. _PyGObject: https://wiki.gnome.org/Projects/PyGObject
.. _GTK+: http://www.gtk.org/
.. _PyAV: https://github.com/mikeboers/PyAV
.. _python-mpv: https://github.com/jaseg/python-mpv
.. _mpv: https://github.com/mpv-player/mpv
.. _Pillow: https://python-pillow.org/


